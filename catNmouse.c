///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  @ <@byrongs@hawaii.edu>
/// @date    @ 1 Feb  2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
int main( int argc, char* argv[] ) {
   int theMaxValue = atoi(argv[1]);
   if (theMaxValue >= 1){
	   int aGuess;
	   int thenumberimthinkingof = rand() % theMaxValue;
   do{	   
	   printf("Ok cat, I'm thinking of a number from 1 to %d. Make a guess: \n", theMaxValue);
           scanf("%d", &aGuess);
	   if (aGuess == thenumberimthinkingof){
		   printf("You got me!\n");                
                   printf("  /\\_/\\\n");
                   printf(" ( o.o )\n");		   
                   printf("  > ^ < \n"); 
	           return 0;
           }
	   else if (aGuess < 1){
		   printf("You must enter a number that's >= 1\n");
           } 
	   else if (aGuess > theMaxValue){
		   printf("You must enter a number that's <= %d\n", theMaxValue);
           }
	   else if (aGuess > thenumberimthinkingof){
		   printf("No cat... the number I'm thinking of is smaller than %d\n", aGuess);
           } 
	   else if (aGuess < thenumberimthinkingof){
		   printf("No cat... the number I'm thinking of is larger than %d\n", aGuess);
	   }



   }while (aGuess != thenumberimthinkingof);
   
   }else{
	    printf("error, invalid entry\n");
	    return 1;
    }
				     }
	 
	  


